# Basic native lib App with JNI

## Project summary

This is the starting point of an App adding C++ native code thanks to the Android Java Native Interface and the Native Development Kit.

## Current Status

Basic App structure.

### Project status

Project is documented in [Jira](https://smog.atlassian.net/jira/software/projects/NFC/boards/4/backlog) and developed using an Agile approach, adding features and fixing bugs as git-flow branches as per the Jira workflow.
Currently it is a basic JNI Android App with the NDK (version 19.2.5345600)

## Built With

* [Jira](https://www.atlassian.com/software/jira) - The #1 software development tool used by agile teams
* [Android](https://developer.android.com/studio) - The fastest tools for building apps on every type of Android device.
* [Kotlin](https://kotlinlang.org/) - A cross-platform, statically typed, general-purpose programming language
* [JNI](https://kotlinlang.org/) - It defines a way for the bytecode that Android compiles from managed code (written in the Java or Kotlin programming languages) to interact with native code (written in C/C++)
* [NDK](https://developer.android.com/ndk/guides) - A set of tools that allows you to use C and C++ code with Android


## Authors

* **Marc Farssac** - *Initial combined work* - [NFC extended Project](https://gitlab.com/marc.farssac.busquets/nsp-and-ndef-near-field-communication-reader)

## License

This project is licensed under the MIT License 

## Acknowledgments

* TapLinx, NXP, Stackoverflow, Android developers